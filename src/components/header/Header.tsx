import React, {Component} from 'react';
import styles from "./Header.module.scss"
export default class App extends Component <{title: string}>{
    render = () => (
        <h2 className={styles.header}>{this.props.title}</h2>
    )
}