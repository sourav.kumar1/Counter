import React, { Component } from 'react';
import Header from "../header/Header"
import styles from "./Incrementer.module.scss"


export default class Incrementer extends Component<{}, { number: number }>{

  constructor(props: any) {
    super(props);
    this.state = {
      number: 0,
    };
  }
  increment = () => {
    this.setState({
      number: this.state.number > 98 ? 0 : this.state.number + 1
    });
  }

  render = () => (
    <>
      <Header title="Counter" />
      <div className={styles.main}>
        <div className={styles.counter}>{this.state.number}</div>
        <button className={styles.btn} onClick={this.increment}>Click</button>
      </div>
    </>
  )
}